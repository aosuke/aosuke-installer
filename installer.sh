#!/bin/bash

set -e
set -u

# aosuke-installer installer script.
# v0.1 - Samuel Dionne-Riel <samuel@dionne-riel.com>
#
# This installer script makes use of built packages.
# It first extracts them to make pacman usable, then
# uses extracted pacman to install said packages.

# Changing this is useless if packages have been built using this
# package folder.
AOSUKE_PATH="/usr/local/aosuke"

# Sanity checking. We cannot install it over itself.
if [ -e "$AOSUKE_PATH" ]; then
	echo "ERROR: $AOSUKE_PATH exists in the filesystem."
	echo "This script won't run until that folder is removed."
	exit 1
fi

# This script sadly needs to be run as root. I do not want
# to call sudo inside the script.
if [[ $UID != "0" ]] ; then
	echo "Script must sadly be run as root"
	echo 'Try : $ sudo '"$0"
	exit 1
fi

# Once, in the past, package files were not globbed from
# the current directory. This alias was more useful then.
untarcmd() {
	tar -x -pf "$@" --exclude ".PKGINFO" --exclude ".MTREE" -C /
}

# This extracts the packages at their destination
for f in *.pkg.tar* ; do
	echo "First pass dirty installing $f"
	untarcmd $f
done

# We add the aosuke prefix
export PATH="$AOSUKE_PATH/bin:$PATH"

# Then, we used the not-yet-installed-but-extracted pacman
# to bootstrap itself. This use of --force is explained by
# the fact that all the files it would install are already
# present in the prefix.
pacman -U --noconfirm --force *.pkg.tar*

pacman -Syyu --noconfirm base

# A last little message to the user.
echo
cat <<EOM
 :: All-righty, the aosuke prefix has been installed.
    It is pretty bare right now. You might want to install
    or build packages.

    You are using the default aosuke configuration, which
    uses a mirrorlist in a package to configure the used
    mirror.

    To use the prefix, prepend its binary path to your path
      > export PATH="$AOSUKE_PATH:\$PATH"
    
    For this experimental release, I would not consider it
    safe to add that folder to /etc/paths or /etc/paths.d
    ;)
EOM

# And goodbye to you, attentive script-reader.
# May aosuke be useful to you!


